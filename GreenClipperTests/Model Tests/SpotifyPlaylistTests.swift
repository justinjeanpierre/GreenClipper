//
//  SpotifyPlaylistTests.swift
//  GreenClipperTests
//

import XCTest

class SpotifyPlaylistTests: XCTestCase {
    
    func testParsePlaylistFromFile() {
        guard let testDataPath = Bundle(for: type(of: self)).path(forResource: "Playlist", ofType: "json") else { XCTAssert(false); return }
        guard let jsonData = FileManager.default.contents(atPath: testDataPath) else { XCTAssert(false); return }

        do {
            let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
            let playlist = SpotifyPlaylist(dictionary: jsonDictionary)

            XCTAssertEqual(jsonDictionary["collaborative"] as? Bool, playlist.collaborative)
//            XCTAssertEqual(jsonDictionary["images"] as? [[String : Any]], playlist.href) // make equatable
            XCTAssertEqual(jsonDictionary["name"] as? String, playlist.name)
//            XCTAssertEqual(jsonDictionary["owner"] as? [String : Any], playlist.type)
            XCTAssertEqual(jsonDictionary["snapshot_id"] as? String, playlist.snapshotId)
//            XCTAssertEqual(jsonDictionary["tracks"] as? [String : Any], playlist.uri)
        } catch {
            print(error.localizedDescription)
            XCTAssert(false, "Could not read json file.")
        }
    }

}
