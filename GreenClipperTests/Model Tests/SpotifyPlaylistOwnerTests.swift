//
//  SpotifyPlaylistOwnerTests.swift
//  GreenClipperTests
//

import XCTest

class SpotifyPlaylistOwnerTests: XCTestCase {
    
    func testParsePlaylistOwnerFromFile() {
        guard let testDataPath = Bundle(for: type(of: self)).path(forResource: "PlaylistOwner", ofType: "json") else { XCTAssert(false); return }
        guard let jsonData = FileManager.default.contents(atPath: testDataPath) else { XCTAssert(false); return }

        do {
            let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]

            let playlistOwner = SpotifyPlaylistOwner(dictionary: jsonDictionary)

            XCTAssertEqual(jsonDictionary["display_name"] as? String, playlistOwner.displayName)
            XCTAssertEqual(jsonDictionary["href"] as? String, playlistOwner.href)
            XCTAssertEqual(jsonDictionary["id"] as? String, playlistOwner.id)
            XCTAssertEqual(jsonDictionary["type"] as? String, playlistOwner.type)
            XCTAssertEqual(jsonDictionary["uri"] as? String, playlistOwner.uri)
        } catch {
            XCTAssert(false, "Could not read json file.")
        }
    }

    func testParsePlaylistOwnerFromFileExtended() {
        guard let testDataPath = Bundle(for: type(of: self)).path(forResource: "PlaylistOwner-ext", ofType: "json") else { XCTAssert(false); return }
        guard let jsonData = FileManager.default.contents(atPath: testDataPath) else { XCTAssert(false); return }

        do {
            let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]

            let playlistOwner = SpotifyPlaylistOwner(dictionary: jsonDictionary)

            XCTAssertEqual((jsonDictionary["owner"] as! [String : Any])["display_name"] as? String, playlistOwner.displayName)
            XCTAssertEqual((jsonDictionary["owner"] as! [String : Any])["href"] as? String, playlistOwner.href)
            XCTAssertEqual((jsonDictionary["owner"] as! [String : Any])["id"] as? String, playlistOwner.id)
            XCTAssertEqual((jsonDictionary["owner"] as! [String : Any])["type"] as? String, playlistOwner.type)
            XCTAssertEqual((jsonDictionary["owner"] as! [String : Any])["uri"] as? String, playlistOwner.uri)
        } catch {
            XCTAssert(false, "Could not read json file.")
        }
    }
    
}
