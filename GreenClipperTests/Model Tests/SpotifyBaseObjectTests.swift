//
//  SpotifyBaseObjectTests.swift
//  GreenClipperTests
//

import XCTest

class SpotifyBaseObjectTests: XCTestCase {

    func testParseBaseObjectFromJSONString() {
        guard let testDataPath = Bundle(for: type(of: self)).path(forResource: "Base", ofType: "json") else { XCTAssert(false); return }
        guard let jsonData = FileManager.default.contents(atPath: testDataPath) else { XCTAssert(false); return }

        do {
            let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]

            let baseObject = SpotifyBaseObject(dictionary: jsonDictionary)

            XCTAssertEqual(jsonDictionary["href"] as? String, baseObject.href)
            XCTAssertEqual(jsonDictionary["id"] as? String, baseObject.id)
            XCTAssertEqual(jsonDictionary["type"] as? String, baseObject.type)
            XCTAssertEqual(jsonDictionary["uri"] as? String, baseObject.uri)
        } catch {
            XCTAssert(false, "Could not read json file.")
        }
    }

}
