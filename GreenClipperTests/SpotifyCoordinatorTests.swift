//
//  SpotifyCoordinatorTests.swift
//  GreenClipperTests
//

import XCTest

@testable import GreenClipper

class SpotifyCoordinatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()

        let auth = SPTAuth.defaultInstance()

        if auth?.session != nil {
            let refreshExpectation = expectation(description: "sessionRefreshExpectation")

            auth?.renewSession(auth?.session, callback: { error, session in
                refreshExpectation.fulfill()
            })

            wait(for: [refreshExpectation], timeout: 10)
        }
    }

//    func testAuthSession() {
//        XCTAssertNil(SPTAuth.defaultInstance().session)
//
//        let sessionExpectation = expectation(description: "testAuthSessionExpectation")
//
//        SpotifyCoordinator.shared.configureAuthentication {
//            XCTAssertNotNil(SPTAuth.defaultInstance().session)
//
//            sessionExpectation.fulfill()
//        }
//
//        wait(for: [sessionExpectation], timeout: 10)
//    }
    
}
