//
//  GreenClipperUITests.swift
//  GreenClipperUITests
//

import XCTest

class GreenClipperUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
    }
    
    func testBrowserDone() {
        // test show/hide login browser
        let app = XCUIApplication()
        app.navigationBars["Green Clipper"].buttons["log in"].tap()
        app/*@START_MENU_TOKEN@*/.otherElements["URL"]/*[[".buttons[\"Address\"]",".otherElements[\"Address\"]",".otherElements[\"URL\"]",".buttons[\"URL\"]"],[[[-1,2],[-1,1],[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
    }

//    func testShowPlaylists() {
//        // test will only run if user is logged in
//        // (either way, Xcode will crash when user taps the
//        // "okay" button during spotify browser log-in.)
//        let app = XCUIApplication()
//        app.buttons["show playlist"].tap()
//        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Hot Hits Canada"]/*[[".cells.staticTexts[\"Hot Hits Canada\"]",".staticTexts[\"Hot Hits Canada\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        app.navigationBars["Hot Hits Canada"].buttons["Back"].tap()
//
//        let greenClipperNavigationBar = app.navigationBars["Green Clipper"]
//        greenClipperNavigationBar.buttons["Green Clipper"].tap()
//        greenClipperNavigationBar.buttons["log out"].tap()
//    }

}
