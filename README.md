# Greenclipper

## Overview

This is a simple [Spotify](https://spotify.com/) track player.
The user can: 
- log in to their Spotify account
- log out of their Spotify account
- view a list of featured Canadian playlists
- select a playlist to view its tracks
	- select a track from the playlist to play
		- play/pause/stop the track's playback

The supplied requirements and specifications are located in requirements-specifications.pdf.  Additionally, the user will need a Spotify premium account.

## Comments

The part about "vous devez développer l’interface graphique programmatiquement (sans storyboard)" could be interpreted as either without storyboards but with individual xibs, or as without any xib files at all.  I chose to avoid xibs altogether.

There were many User Interface details that were left unaddressed as they did not seem as important as overall functionality and code quality.  The UI tests are for illustrative purposes only.

## Build and run

Dependency management is through [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).  There was no Cocoapod available for the Spotify SDK, but I still wanted something better than “download, drag, and drop.”

You will need to run `git submodule init` and/or `git submodule update` before building if the /ios-sdk directory is empty.
