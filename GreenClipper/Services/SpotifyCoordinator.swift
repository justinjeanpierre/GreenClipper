//
//  SpotifyCoordinator.swift
//  GreenClipper
//

class SpotifyCoordinator {

    // MARK: - Properties
    static let shared = SpotifyCoordinator()

    // MARK: - Private properties
    private let SESSION_USER_DEFAULTS_KEY = "session_user_defaults_key"
    private var auth: SPTAuth?

    // MARK: - Public methods
    func configureAuthentication(completion: (() -> Void)? = nil) {
        auth = SPTAuth.defaultInstance()
        auth?.clientID = "46e0f3f0f9304a2a99e37d9667b2efed"
        auth?.redirectURL = URL(string: "greenclipper://spotify/")
        auth?.sessionUserDefaultsKey = SESSION_USER_DEFAULTS_KEY
        auth?.requestedScopes = [SPTAuthStreamingScope]

        do {
            try SPTAudioStreamingController.sharedInstance().start(withClientId: auth?.clientID)
        } catch {
            print(#function)
            print(error.localizedDescription)
        }

        DispatchQueue.main.async {
            completion?()
        }
    }

}
