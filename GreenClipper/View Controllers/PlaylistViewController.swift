//
//  PlaylistViewController.swift
//  GreenClipper
//

import UIKit

class PlaylistViewController: UIViewController {

    // MARK: - Properties
    var playlist: SpotifyPlaylist? {
        didSet {
            getPlaylistTracks()
        }
    }

    // MARK: - Private properties
    private var tracklistTableView: UITableView?
    private var tracks = [SpotifyTrack]()
    private var tracklistCellReuseIdentifier = "tracklistCellReuseIdentifier"
    private var activityIndicator: UIActivityIndicatorView?

    // MARK: UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        tracklistTableView = UITableView(frame: view.frame, style: UITableViewStyle.plain)
        tracklistTableView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        view.addSubview(tracklistTableView!)

        tracklistTableView?.dataSource = self
        tracklistTableView?.delegate = self

        // UI configuration
        activityIndicator = UIActivityIndicatorView()
        activityIndicator?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator?.autoresizingMask = [UIViewAutoresizing.flexibleTopMargin,
                                              UIViewAutoresizing.flexibleRightMargin,
                                              UIViewAutoresizing.flexibleBottomMargin,
                                              UIViewAutoresizing.flexibleLeftMargin]
        activityIndicator?.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator?.hidesWhenStopped = true
        view.addSubview(activityIndicator!)

        let activityIndicatorViews = ["spinner" : activityIndicator as Any]
        let horizontalActivityIndicatorFormatString = "H:|-[spinner(>=30)]-|"
        let horizontalActivityIndicatorConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalActivityIndicatorFormatString,
                                                                                    options: NSLayoutFormatOptions.alignAllCenterY,
                                                                                    metrics: nil,
                                                                                    views: activityIndicatorViews)
        NSLayoutConstraint.activate(horizontalActivityIndicatorConstraints)

        let verticalactivityIndicatorButtonFormatString = "V:|-[spinner(>=30)]-|"
        let verticalactivityIndicatorConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalactivityIndicatorButtonFormatString,
                                                                                  options: NSLayoutFormatOptions.alignAllCenterX,
                                                                                  metrics: nil,
                                                                                  views: activityIndicatorViews)
        NSLayoutConstraint.activate(verticalactivityIndicatorConstraints)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let selectedRowIndexPath = tracklistTableView?.indexPathForSelectedRow {
            tracklistTableView?.deselectRow(at: selectedRowIndexPath, animated: true)
        }
    }

    private func getPlaylistTracks() {
        activityIndicator?.startAnimating()
        do {
            guard let playlistURIString = playlist?.uri else { return }
            guard let playlistURIURL = URL(string: playlistURIString) else { return }
            let request = try SPTPlaylistSnapshot.createRequestForPlaylist(withURI: playlistURIURL,
                                                                           accessToken: SPTAuth.defaultInstance().session.accessToken)
            URLSession.shared.dataTask(with: request, completionHandler: { data, urlResponse, error in
                if let responseData = data {
                    do {
                        let jsonDictionary = try JSONSerialization.jsonObject(with: responseData,
                                                                              options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]

                        let trackDictionary = jsonDictionary["tracks"] as! [String : Any]
                        let itemsArray = trackDictionary["items"] as! [[String : Any]]

                        for item in itemsArray {
                            self.tracks.append(SpotifyTrack(dictionary: item))
                        }

                        DispatchQueue.main.async {
                            self.tracklistTableView?.reloadData()
                        }
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                }

                DispatchQueue.main.async {
                    self.activityIndicator?.stopAnimating()
                }

            }).resume()
        } catch {
            print(error.localizedDescription)

            activityIndicator?.stopAnimating()
        }
    }
}

// MARK: - UITableViewDatasource methods
extension PlaylistViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell? =
            tracklistTableView?.dequeueReusableCell(withIdentifier: tracklistCellReuseIdentifier) ??
            UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: tracklistCellReuseIdentifier)

        cell?.textLabel?.text = tracks[indexPath.row].trackName

        if let artistName = tracks[indexPath.row].artistName {
            cell?.detailTextLabel?.text = artistName
        }

        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

        return cell!
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

// MARK: - UITableViewDelegate methods
extension PlaylistViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playbackViewController = PlaybackViewController()

        navigationController?.pushViewController(playbackViewController, animated: true)

        playbackViewController.track = tracks[indexPath.row]
    }
}
