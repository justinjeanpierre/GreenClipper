//
//  AuthViewController.swift
//  GreenClipper
//

import UIKit

class AuthViewController: UIViewController {

    // MARK: - Private properties
    private var loginLogoutButton: UIBarButtonItem?
    private var activityIndicator: UIActivityIndicatorView?
    private var showPlaylistButton: UIButton?
    private var auth = SPTAuth.defaultInstance()
    private var isLoggedIn: Bool {
        return SPTAudioStreamingController.sharedInstance().loggedIn
    }

    // MARK: - UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()

        configureInterfaceElements()
        configureNotifications()

        SPTAudioStreamingController.sharedInstance().delegate = self

        SpotifyCoordinator.shared.configureAuthentication()

        if isLoggedIn {
            loginLogoutButton?.title = NSLocalizedString("log out", comment: "")
        } else {
            loginLogoutButton?.title = NSLocalizedString("log in", comment: "")
        }

        showPlaylistButton?.isHidden = !(isLoggedIn)
    }

    // MARK: - Spotify authentication
    @objc func loginLogout(_ sender: UIBarButtonItem!) {
        activityIndicator?.startAnimating()
        
        if isLoggedIn {
            SPTAudioStreamingController.sharedInstance().logout()
        } else {
            if let spotifyWebAuthenticationURL = self.auth?.spotifyWebAuthenticationURL() {
                let authenticationViewController = SFSafariViewController(url: spotifyWebAuthenticationURL)
                self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(authenticationViewController, animated: true)
            }
        }
    }

    // MARK: - Private methods
    private func configureInterfaceElements() {
        title = "Green Clipper"

        view.backgroundColor = .lightGray

        loginLogoutButton = UIBarButtonItem(title: NSLocalizedString("log in", comment: ""),
                                            style: UIBarButtonItemStyle.plain,
                                            target: self,
                                            action: #selector(loginLogout(_:)))
        navigationItem.rightBarButtonItem = loginLogoutButton

        activityIndicator = UIActivityIndicatorView()
        activityIndicator?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator?.autoresizingMask = [UIViewAutoresizing.flexibleTopMargin,
                                               UIViewAutoresizing.flexibleRightMargin,
                                               UIViewAutoresizing.flexibleBottomMargin,
                                               UIViewAutoresizing.flexibleLeftMargin]
        activityIndicator?.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator?.hidesWhenStopped = true
        view.addSubview(activityIndicator!)

        let activityIndicatorViews = ["spinner" : activityIndicator as Any]
        let horizontalActivityIndicatorFormatString = "H:|-[spinner(>=30)]-|"
        let horizontalActivityIndicatorConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalActivityIndicatorFormatString,
                                                                                 options: NSLayoutFormatOptions.alignAllCenterY,
                                                                                 metrics: nil,
                                                                                 views: activityIndicatorViews)
        NSLayoutConstraint.activate(horizontalActivityIndicatorConstraints)

        let verticalActivityIndicatorButtonFormatString = "V:|-[spinner(>=30)]-|"
        let verticalActivityIndicatorConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalActivityIndicatorButtonFormatString,
                                                                               options: NSLayoutFormatOptions.alignAllCenterX,
                                                                               metrics: nil,
                                                                               views: activityIndicatorViews)
        NSLayoutConstraint.activate(verticalActivityIndicatorConstraints)

        showPlaylistButton = UIButton()
        showPlaylistButton?.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleLeftMargin, UIViewAutoresizing.flexibleRightMargin]
        showPlaylistButton?.translatesAutoresizingMaskIntoConstraints = false

        showPlaylistButton?.setTitle(NSLocalizedString("Show playlist", comment: ""), for: UIControlState.normal)
        showPlaylistButton?.addTarget(self, action: #selector(showPlaylistListViewController(_:)), for: UIControlEvents.touchUpInside)

        view.addSubview(showPlaylistButton!)

        let playlistButtonViews = ["playlist" : showPlaylistButton as Any]
        let horizontalPlaylistButtonFormatString = "H:|-[playlist(>=120)]-|"
        let horizontalPlaylistButtonConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalPlaylistButtonFormatString,
                                                                   options: NSLayoutFormatOptions.alignAllCenterY,
                                                                   metrics: nil,
                                                                   views: playlistButtonViews)
        NSLayoutConstraint.activate(horizontalPlaylistButtonConstraints)

        let verticalPlaylistButtonFormatString = "V:|-[playlist(>=20)]-|"
        let verticalPlaylistButtonConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalPlaylistButtonFormatString,
                                                                   options: NSLayoutFormatOptions.alignAllCenterX,
                                                                   metrics: nil,
                                                                   views: playlistButtonViews)
        NSLayoutConstraint.activate(verticalPlaylistButtonConstraints)
    }

    @objc private func showPlaylistListViewController(_ sender: UIButton?) {
        let playlistListViewController = PlaylistListViewController()
        navigationController?.pushViewController(playlistListViewController, animated: true)
    }
}

// MARK: - Notification handlers
extension AuthViewController {
    fileprivate func configureNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissAuthenticationController),
                                               name: NSNotification.Name(NOTIFICATION_NAME_URL),
                                               object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(loginPlayer),
                                               name: NSNotification.Name(NOTIFICATION_NAME_AUTH),
                                               object: nil)
    }

    @objc fileprivate func dismissAuthenticationController() {
        dismiss(animated: true)
    }

    @objc fileprivate func loginPlayer() {
        SPTAudioStreamingController.sharedInstance().login(withAccessToken: auth?.session.accessToken)
    }
}

// MARK: - SPTAudioStreamingDelegate methods
extension AuthViewController: SPTAudioStreamingDelegate {
    // not all necessary, but interesting
    // to see if/when they are called
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        print(#function)

        loginLogoutButton?.title = NSLocalizedString("log out", comment: "")
        showPlaylistButton?.isHidden = !(isLoggedIn)
        activityIndicator?.stopAnimating()
    }

    func audioStreamingDidLogout(_ audioStreaming: SPTAudioStreamingController!) {
        print(#function)

        loginLogoutButton?.title = NSLocalizedString("log in", comment: "")
        showPlaylistButton?.isHidden = !(isLoggedIn)
        activityIndicator?.stopAnimating()
    }

    func audioStreamingDidReconnect(_ audioStreaming: SPTAudioStreamingController!) {
        print(#function)
    }

    func audioStreamingDidDisconnect(_ audioStreaming: SPTAudioStreamingController!) {
        print(#function)
    }

    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didReceiveError error: Error!) {
        print(#function)
        print(error.localizedDescription)
    }

    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didReceiveMessage message: String!) {
        print(#function)
        print(message)
    }

    func audioStreamingDidEncounterTemporaryConnectionError(_ audioStreaming: SPTAudioStreamingController!) {
        print(#function)
    }
}
