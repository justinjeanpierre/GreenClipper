//
//  PlaylistListViewController.swift
//  GreenClipper
//

import UIKit

class PlaylistListViewController: UIViewController {

    // MARK: - Private properties
    private var playlistListTableView: UITableView?
    private var playlistList = [SpotifyPlaylist]()
    private var playlistListCellReuseIdentifier = "playlistListCellReuseIdentifier"

    // MARK: - UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()

        playlistListTableView = UITableView(frame: view.frame, style: UITableViewStyle.plain)
        playlistListTableView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        playlistListTableView?.register(UITableViewCell.self, forCellReuseIdentifier: playlistListCellReuseIdentifier)
        view.addSubview(playlistListTableView!)

        playlistListTableView?.dataSource = self
        playlistListTableView?.delegate = self
        playlistListTableView?.allowsMultipleSelection = false

        getPlaylists()
    }

    // MARK: - Private methods
    private func getPlaylists() {
        do {
            let urlRequest = try SPTBrowse.createRequestForFeaturedPlaylists(inCountry: "CA",
                                                                             limit: 50,
                                                                             offset: 0,
                                                                             locale: "en_CA",
                                                                             timestamp: Date(),
                                                                             accessToken: SPTAuth.defaultInstance().session.accessToken)
            URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, urlResponse, error in
                if let playlistData = data {
                    do {
                        let playlistResponse = try SPTFeaturedPlaylistList.init(from: playlistData, with: urlResponse)
                        print(playlistResponse.message)

                        let jsonDictionary = try JSONSerialization.jsonObject(with: playlistData,
                                                                              options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]

                        guard let playlistsDictionary = jsonDictionary["playlists"] as? [String : Any] else { return }
                        guard let itemsArray = playlistsDictionary["items"] as? [[String : Any]] else { return }

                        for item in itemsArray {
                            self.playlistList.append(SpotifyPlaylist(dictionary: item as [String : Any]))
                        }

                        DispatchQueue.main.async {
                            self.playlistListTableView?.reloadData()
                        }

                    } catch {
                        print("error parsing playlist")
                    }
                } else {
                    print("error parsing response data")
                }
            }).resume()
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK: - UITableViewDataSource methods
extension PlaylistListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlistList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: playlistListCellReuseIdentifier, for: indexPath)

        cell.textLabel?.text = playlistList[indexPath.row].name
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

        return cell
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

// MARK: - UITableViewDelegate methods
extension PlaylistListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tracklistViewController = PlaylistViewController()
        
        tracklistViewController.playlist = playlistList[indexPath.row]

        navigationController?.pushViewController(tracklistViewController, animated: true)
    }
}
