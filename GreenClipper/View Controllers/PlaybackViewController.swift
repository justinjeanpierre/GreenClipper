//
//  PlaybackViewController.swift
//  GreenClipper
//

import UIKit

class PlaybackViewController: UIViewController {
    // MARK: - Properties
    var track: SpotifyTrack? {
        didSet {
            title = track?.trackName ?? ""
        }
    }

    // MARK: - Private properties
    private var albumImageView: UIImageView?
    private var trackTitleLabel: UILabel?
    private var artistTitleLabel: UILabel?
    private var playButton: UIButton?
    private var pauseButton: UIButton?
    private var stopButton: UIButton?
    private var playbackPositionSlider: UISlider?

    // MARK: - UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()

        SPTAudioStreamingController.sharedInstance().playbackDelegate = self

        configureInterfaceElements()

        loadCurrentTrack()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        DispatchQueue.main.async {
            self.pauseCurrentTrack()
        }
    }

    // MARK: - Private methods
    private func configureInterfaceElements() {
        view.backgroundColor = .white

        // configure image
        albumImageView = UIImageView()
        albumImageView?.layer.cornerRadius = 8.0
        albumImageView?.layer.masksToBounds = true
        albumImageView?.backgroundColor = .lightGray
        albumImageView?.contentMode = UIViewContentMode.scaleAspectFill

        albumImageView?.autoresizingMask = [UIViewAutoresizing.flexibleTopMargin,
                                            UIViewAutoresizing.flexibleRightMargin,
                                            UIViewAutoresizing.flexibleLeftMargin,
                                            UIViewAutoresizing.flexibleHeight,
                                            UIViewAutoresizing.flexibleWidth]
        albumImageView?.translatesAutoresizingMaskIntoConstraints = false

        // TODO: replace with SDWebImage
        if let imageURLString = track?.imageURL {
            if let imageURL = URL(string: imageURLString) {
                URLSession.shared.dataTask(with: imageURL, completionHandler: { data, urlResponse, error in
                    if let responseData = data {
                        DispatchQueue.main.async {
                            self.albumImageView?.image = UIImage(data: responseData)
                        }
                    }
                }).resume()
            }
        }

        // configure track info
        trackTitleLabel = UILabel()
        trackTitleLabel?.autoresizingMask = [UIViewAutoresizing.flexibleRightMargin,
                                             UIViewAutoresizing.flexibleLeftMargin,
                                             UIViewAutoresizing.flexibleWidth]
        trackTitleLabel?.translatesAutoresizingMaskIntoConstraints = false
        trackTitleLabel?.textAlignment = .center
        trackTitleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        trackTitleLabel?.text = track?.trackName ?? ""

        // configure artist info
        artistTitleLabel = UILabel()
        artistTitleLabel?.autoresizingMask = [
                                              UIViewAutoresizing.flexibleRightMargin,
                                              UIViewAutoresizing.flexibleLeftMargin,
                                              UIViewAutoresizing.flexibleWidth]
        artistTitleLabel?.translatesAutoresizingMaskIntoConstraints = false
        artistTitleLabel?.textAlignment = .center
        artistTitleLabel?.text = track?.artistName ?? ""

        view.addSubview(albumImageView!)
        view.addSubview(trackTitleLabel!)
        view.addSubview(artistTitleLabel!)

        // configure play/pause/stop buttons
        pauseButton = UIButton(type: UIButtonType.custom)
        pauseButton?.setImage(UIImage(named: "grey-pause"), for: UIControlState.normal)
        pauseButton?.addTarget(self, action: #selector(pauseCurrentTrack), for: UIControlEvents.touchUpInside)

        playButton = UIButton(type: UIButtonType.custom)
        playButton?.setImage(UIImage(named: "grey-play"), for: UIControlState.normal)
        playButton?.addTarget(self, action: #selector(playCurrentTrack), for: UIControlEvents.touchUpInside)

        stopButton = UIButton(type: UIButtonType.custom)
        stopButton?.setImage(UIImage(named: "grey-stop"), for: UIControlState.normal)
        stopButton?.addTarget(self, action: #selector(stopCurrentTrack), for: UIControlEvents.touchUpInside)

        let buttonStack = UIStackView(arrangedSubviews: [playButton!, pauseButton!, stopButton!])
        buttonStack.axis = UILayoutConstraintAxis.horizontal
        buttonStack.distribution = UIStackViewDistribution.fillEqually
        buttonStack.alignment = UIStackViewAlignment.center
        buttonStack.spacing = 8.0

        buttonStack.autoresizingMask = [UIViewAutoresizing.flexibleRightMargin,
                                        UIViewAutoresizing.flexibleLeftMargin,
                                        UIViewAutoresizing.flexibleBottomMargin,
                                        UIViewAutoresizing.flexibleWidth]
        buttonStack.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(buttonStack)

        playbackPositionSlider = UISlider()
        playbackPositionSlider?.autoresizingMask = [ UIViewAutoresizing.flexibleTopMargin,
                                               UIViewAutoresizing.flexibleRightMargin,
                                               UIViewAutoresizing.flexibleBottomMargin,
                                               UIViewAutoresizing.flexibleLeftMargin,
                                               UIViewAutoresizing.flexibleWidth,
                                               UIViewAutoresizing.flexibleHeight]
        playbackPositionSlider?.translatesAutoresizingMaskIntoConstraints = false
        playbackPositionSlider?.minimumTrackTintColor = UIColor(red: 0.0, green: 217.0/255.0, blue: 90.0/255.0, alpha: 1.0)
        playbackPositionSlider?.maximumTrackTintColor = UIColor.black
        playbackPositionSlider?.setThumbImage(UIImage(), for: UIControlState.normal) // "hide" thumb

        view.addSubview(playbackPositionSlider!)

        // configure playback slider
        if let trackDuration = track?.duration {
            playbackPositionSlider?.maximumValue = Float(trackDuration)
        }

        let verticalAllViews = ["imageView" : albumImageView as Any,
                             "title" : trackTitleLabel as Any,
                             "artist" : artistTitleLabel as Any,
                             "buttons" : buttonStack as Any,
                             "slider" : playbackPositionSlider as Any]
        let verticalViewsFormatString = "V:|-[imageView(>=80,<=200)]-[title]-[artist]-[buttons]-[slider]-(>=20)-|"
        let verticalViewsConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalViewsFormatString,
                                                                      options: NSLayoutFormatOptions.alignAllCenterX,
                                                                      metrics: nil,
                                                                      views: verticalAllViews)
        NSLayoutConstraint.activate(verticalViewsConstraints)

        let horizontalViews = ["slider" : playbackPositionSlider as Any]
        let horizontalViewsFormatString = "H:|-[slider]-|"
        let horizontalViewsConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalViewsFormatString,
                                                                      options: NSLayoutFormatOptions.directionLeftToRight,
                                                                      metrics: nil,
                                                                      views: horizontalViews)
        NSLayoutConstraint.activate(horizontalViewsConstraints)

        let horizontalImageView = ["imageView" : albumImageView as Any]
        let horizontalImageViewFormatString = "H:|-[imageView]-|"
        let horizontalImageViewConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalImageViewFormatString,
                                                                        options: NSLayoutFormatOptions.directionLeftToRight,
                                                                        metrics: nil,
                                                                        views: horizontalImageView)
        NSLayoutConstraint.activate(horizontalImageViewConstraints)
    }

    // MARK: - Track playback controls
    private func loadCurrentTrack() {
        guard let trackURLString = track?.url else { return }
        SPTAudioStreamingController.sharedInstance().playSpotifyURI(trackURLString, startingWith: 0, startingWithPosition: 0) { error in
            if error != nil {
                print(error?.localizedDescription as Any)
            }

            SPTAudioStreamingController.sharedInstance().setIsPlaying(false) { error in
                if error != nil {
                    print(#function)
                    print(error!.localizedDescription)
                }
            }
        }
    }

    @objc private func playCurrentTrack() {
        if SPTAudioStreamingController.sharedInstance().playbackState.isPlaying == false {
            SPTAudioStreamingController.sharedInstance().setIsPlaying(true) { error in
                if error != nil {
                    print(#function)
                    print(error!.localizedDescription)
                }
            }
        }
    }

    @objc private func pauseCurrentTrack() {
        if SPTAudioStreamingController.sharedInstance().playbackState.isPlaying == true {
            SPTAudioStreamingController.sharedInstance().setIsPlaying(false) { error in
                if error != nil {
                    print(#function)
                    print(error!.localizedDescription)
                }
            }
        }
    }

    @objc private func stopCurrentTrack() {
        // go to 0 time index, then pause
        SPTAudioStreamingController.sharedInstance().seek(to: 0) { error in
            if error != nil {
                print(#function)
                print(error!.localizedDescription)
            }

            // update slider position
            self.playbackPositionSlider?.setValue(0.0, animated: true)

            SPTAudioStreamingController.sharedInstance().setIsPlaying(false) { error in
                if error != nil {
                    print(#function)
                    print(error!.localizedDescription)
                }
            }
        }
    }
}

// MARK: - SPTAudioStreamingPlaybackDelegate methods
extension PlaybackViewController: SPTAudioStreamingPlaybackDelegate {
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangePosition position: TimeInterval) {
        playbackPositionSlider?.setValue(Float(position), animated: true)
    }
}
