//
//  AppDelegate.swift
//  GreenClipper
//
//  Created by Justin Jean-Pierre on 2017-12-29.
//  Copyright © 2017 Jean-Pierre Digital. All rights reserved.
//

import UIKit

let NOTIFICATION_NAME_URL = "url_notification_name"
let NOTIFICATION_NAME_AUTH = "auth_notification_name"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: AuthViewController())
        window?.makeKeyAndVisible()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        if SPTAudioStreamingController.sharedInstance().playbackState.isPlaying == true {
            SPTAudioStreamingController.sharedInstance().setIsPlaying(false) { (error) in
                if error != nil {
                    print(#function)
                    print(error!.localizedDescription)
                }

                do {
                    try SPTAudioStreamingController.sharedInstance().stop()
                } catch {
                    print(#function)
                    print(error.localizedDescription)
                }
            }
        }
    }

    // MARK: - Spotify authentication methods
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if SPTAuth.defaultInstance().canHandle(url) {
            let notification = Notification(name: Notification.Name(NOTIFICATION_NAME_URL), object: nil, userInfo: options)
            NotificationCenter.default.post(notification)

            SPTAuth.defaultInstance().handleAuthCallback(withTriggeredAuthURL: url) { error, session in
                if session != nil {
                    let loginNotification = Notification(name: Notification.Name(NOTIFICATION_NAME_AUTH), object: nil, userInfo: nil)
                    NotificationCenter.default.post(loginNotification)
                }
            }

            return true
        }

        return false
    }

}
