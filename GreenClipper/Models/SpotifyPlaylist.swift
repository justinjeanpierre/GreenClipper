//
//  SpotifyPlaylist.swift
//  GreenClipper
//

class SpotifyPlaylist: SpotifyBaseObject {
    // MARK: - Properties
    var collaborative: Bool?
    var images: [[String : Any]]?
    var name: String?
    var owner: SpotifyPlaylistOwner?
    var snapshotId: String?
    var tracks: [String : Any]?

    // MARK: - init
    override init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)

        if let _collaborative = dictionary["collaborative"] as? Bool {
            collaborative = _collaborative
        }

        if let _images = dictionary["images"] as? [[String : Any]] {
            images = _images
        }

        if let _name = dictionary["name"] as? String {
            name = _name
        }

        if let _owner = dictionary["owner"] as? [String : Any] {
            owner = SpotifyPlaylistOwner(dictionary: _owner)
        }

        if let _snapshotId = dictionary["snapshot_id"] as? String {
            snapshotId = _snapshotId
        }

        if let _tracks = dictionary["tracks"] as? [String : Any] {
            tracks = _tracks
        }
    }
}
