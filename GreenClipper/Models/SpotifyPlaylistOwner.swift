//
//  SpotifyPlaylistOwner.swift
//  GreenClipper
//

class SpotifyPlaylistOwner: SpotifyBaseObject {
    // MARK: - Properties
    var displayName: String?

    // MARK: - init
    override init(dictionary: [String : Any]) {
        var innerDictionary: [String : Any] = dictionary

        if let ownerDictionary = dictionary["owner"] as? [String : Any] {
            innerDictionary = ownerDictionary
        }

        super.init(dictionary: innerDictionary)

        if let _displayName = innerDictionary["display_name"] as? String {
            displayName = _displayName
        }
    }
}
