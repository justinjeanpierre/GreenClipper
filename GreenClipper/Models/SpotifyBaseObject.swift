//
//  SpotifyBaseObject.swift
//  GreenClipper
//

class SpotifyBaseObject {
    // MARK: - Properties
    var externalURLs: [String : String]?
    var href: String?
    var id: String?
    var type: String?
    var uri: String?

    // MARK: - init
    init(dictionary: [String : Any]) {
        if let _href = dictionary["href"] as? String {
            href = _href
        }

        if let _id = dictionary["id"] as? String {
            id = _id
        }

        if let _type = dictionary["type"] as? String {
            type = _type
        }

        if let _uri = dictionary["uri"] as? String {
            uri = _uri
        }
    }
}
