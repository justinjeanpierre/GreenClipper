//
//  SpotifyTrack.swift
//  GreenClipper
//

class SpotifyTrack: SpotifyBaseObject {
    // MARK: - Properties
    var albumName: String?
    var artistName: String?
    var trackName: String?

    var duration: Int?
    var imageURL: String?
    var url: String?

    // MARK: - init
    override init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)

        if let trackDictionary = dictionary["track"] as? [String: Any] {
            if let _trackName = trackDictionary["name"] as? String {
                trackName = _trackName
            }

            if let duration_milliseconds = trackDictionary["duration_ms"] as? Int {
                duration = (duration_milliseconds / 1000) // convert to seconds
            }

            if let uri = trackDictionary["uri"] as? String {
                url = uri
            }

            if let albumDictionary = trackDictionary["album"] as? [String : Any] {
                if let _albumName = albumDictionary["name"] as? String {
                    albumName = _albumName
                }

                if let artistArray = albumDictionary["artists"] as? [[String : Any]] {
                    if let name = artistArray[0]["name"] as? String {
                        artistName = name
                    }
                }

                if let imagesArray = albumDictionary["images"] as? [[String : Any]] {
                    if imagesArray.count > 0 {
                        imageURL = imagesArray[0]["url"] as? String
                    }
                }

            }
        }
    }

}
